OCaml starter package for Ultimate TicTacToe
==============================================================

see theaigames.com for details of the game
------------------------------------------

The provided "main.ml" file is a minimal bot which chooses a random move from the list of legal moves.

As seen in that example, your bot should provide a function which takes a game_state as its argument, and calls issue_order once.

You then provide this function as an argument to the Ulttt.run_bot function.

Have a look at td.ml to see the definition of the game_state data type.

Please note that at this stage, the time_elapsed_this_turn and time_remaining functions have not been thoroughly tested.

